const POSTGRES_URL = require('./connection.json'); 
const { Client, Pool } = require('pg');

const pool = new Pool();

var dbClient = new Client(POSTGRES_URL);

exports.bulkInsert = async (table, columns, data) => {
    const client = new Pool(POSTGRES_URL);
    try {
        await client.query('BEGIN')
        const queryText = `insert into ${table}("${columns.join('","')}") values ${data.join(',')}`
        const res = await client.query(queryText)
        console.log("Operation Result: ",res.command, res.rowCount);
        await client.query('COMMIT')
      } catch (e) {
        await client.query('ROLLBACK')
        console.log(e.message,"Table: ", table)
      } finally {
        await client.end()
      } 
}

exports.querySQL = async function(commands) {
    const client = new Pool(POSTGRES_URL);
    for ( let command of commands){

        try {
            await client.query('BEGIN')
            const res = await client.query(command)
            await client.query('COMMIT')
            console.log("Operation Result: ",res.command, res.rowCount);
        } catch (e) {
            await client.query('ROLLBACK')
            console.log(e.message)
        } finally {
        }
    }
    client.end()
}

exports.singleInsert = async function(table, columns, data) {
    const client = await new Pool.connect(POSTGRES_URL);
    try {
        await client.query('BEGIN')
        const queryText = `insert into ${table}("${columns.join('","')}") values ${data}`
        console.log(queryText);
        const res = await client.query(queryText)
        console.log(res)
        await client.query('COMMIT')
      } catch (e) {
        await client.query('ROLLBACK')
        console.log(e.message, e.stack, table, data)
        return "Already Exists"
      } finally {
        await client.end()
      } 
}

  

exports.dbClient = dbClient;