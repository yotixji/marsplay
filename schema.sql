drop table if exists users cascade;
drop table if exists address;
drop table if exists company;
drop table if exists posts cascade;
drop table if exists comments;

create table users (
    id serial primary key,
    name varchar(30),
    username varchar(30) not null unique,
    email varchar(50) not null unique,
    phone varchar(40),
    website varchar(50)
);

create table address (
    id serial primary key,
    street varchar(40),
    suite  varchar(40),
    city varchar(40) not null,
    zipcode varchar(20),
    userid int not null,
    foreign key (userid) references users (id)
);

create table company (
    id serial primary key,
    name varchar(40) not null,
    catchphrase varchar(50),
    bs varchar(50),
    userid int not null,
    foreign key (userid) references users (id)

);

create table posts (
    id serial primary key,
    title varchar(100) not null,
    body text not null,
    userid int not null,
    foreign key (userid) references users (id)
);

create table comments (
    id serial primary key,
    name varchar(100) not null,
    email varchar(100) not null,
    body text not null,
    postid int not null,
    foreign key (postid) references posts (id) 
);