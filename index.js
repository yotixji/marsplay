const httpClient = require('axios');

const { bulkInsert, singleInsert } = require('./db');

const url = "https://jsonplaceholder.typicode.com";

// Weird things happens sometimes the API return foreign key as userId this function tackles that to convert to user_id
function refineColumns(columns) {
    console.log("Columns: ",columns)
    return columns.map(item => 
        item.toLowerCase().includes('Id') && item.length>2 ?
        item.toLowerCase().replace('id',"_id") : item.toLowerCase().replace('_','')
        )
}

async function addToDb(table, data) {
    const columns = refineColumns(Object.keys(data[0]));
    console.log(`Adding data to Datbase`)
    const rows = data.map(item => {
        const arr = Object.values(item);
        const value = arr.map(item =>{
            if (Number.isInteger(item)) {
                return Number(item)
            } 
            return item.replace(/\//g, '');
        })
        return "('"+arr.join("','")+"')";
    })
    await bulkInsert(table, columns, rows)
    .then(res=>console.log(res))
    .catch(err=>console.log(err));
}

async function main(){
    for (var item of ["/users","/posts","/comments"]) {
        console.log("Getting data from " + item);
        await httpClient.get( url + item)
        .then(resp=>resp.data)
        .then(data=> {
            if (item.slice(1) === 'users') {
                const addresses = [];
                const companies = [];
                // remove above Keys
                const users = data.map(item=>{
                    delete item.address.geo
                    addresses.push({...item.address, user_id: item.id})
                    companies.push({...item.company, user_id: item.id})
                    delete item.address
                    delete item.company
                    return item;
                })
                addToDb(item.slice(1),users); 
                addToDb('address',addresses); 
                addToDb('company',companies); 
            }
            else {
                addToDb(item.slice(1),data); 
            }
        })
        .catch(err=>console.log(err.message));
    }
}

main();
