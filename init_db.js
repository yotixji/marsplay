const fs = require('fs');
const { querySQL } = require('./db');

const schema = fs.readFileSync('./schema.sql').toString().split(';');
querySQL(schema)
.then(res=>console.log("Finished Processing"))
.catch(err=>console.log(err));